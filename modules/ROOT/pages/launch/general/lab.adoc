ifndef::imagesdir[:imagesdir: ../../../images]
:experimental:

以下のコマンドを実行する

[source, console]
----
jupyter lab --no-browser
----

ifdef::backend-revealjs[=== !]
ifndef::backend-revealjs[---]

以下のような画面が表示されるまで待つ 
[source, console, data-cc=false]
----
(sage10) username@hostname:~/docs$ jupyter lab --no-browser

省略

[I 2024-03-21 19:52:29.212 ServerApp] Jupyter Server 2.13.0 is running at:
[I 2024-03-21 19:52:29.213 ServerApp] http://localhost:8888/lab?token=22f9c23fd52365a69b6bcf8e5df20eb23b9cdd709f626830
[I 2024-03-21 19:52:29.213 ServerApp]     http://127.0.0.1:8888/lab?token=22f9c23fd52365a69b6bcf8e5df20eb23b9cdd709f626830
[I 2024-03-21 19:52:29.213 ServerApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 2024-03-21 19:52:29.216 ServerApp]

    To access the server, open this file in a browser:
        file:///home/username/.local/share/jupyter/runtime/jpserver-151782-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/lab?token=22f9c23fd52365a69b6bcf8e5df20eb23b9cdd709f626830
        http://127.0.0.1:8888/lab?token=22f9c23fd52365a69b6bcf8e5df20eb23b9cdd709f626830

省略

----