ifndef::imagesdir[:imagesdir: ../../../images]
:experimental:

ifdef::backend-revealjs[== SageMath Shellの起動方法]
ifndef::backend-revealjs[= SageMath Shellの起動方法]

ifdef::backend-revealjs[=== !]
ifndef::backend-revealjs[---]

include::../../../partials/open_env/macos.adoc[]

ifdef::backend-revealjs[=== !]
ifndef::backend-revealjs[---]

include::../general/shell.adoc[]

