ifndef::imagesdir[:imagesdir: ../../../images]
:experimental:

入手したUbuntuのインストーラーをダブルクリックする +
※ 名前が`Ubuntu`から始まる種類が`APPXファイル`のファイルです。

image::click_ubuntu_installer.png[]

ifdef::backend-revealjs[=== !]
ifndef::backend-revealjs[---]

以下のどちらの画面になっているか確認してください

[cols="a,a"]
|===

|image::already_installed_ubuntu.png[]
|image::install_ubuntu.png[]

|===

ifdef::backend-revealjs[]

<<download_ubuntu_already_installed_ubuntu, 左の画面の場合はここをクリック>> +
<<download_ubuntu_already_installed_ubuntu, 右の画面の場合はここをクリック>>

=== !

不正な操作です +
kbd:[↑]を押して前の手順に戻ってください

=== !

不正な操作です +
kbd:[↑]を押して前の手順に戻ってください

[%notitle, id=download_ubuntu_already_installed_ubuntu]
=== download_ubuntu_already_installed_ubuntu

include::./already_installed.adoc[]

=== !

include::./next.adoc[]

=== !

不正な操作です +
kbd:[↑]を押して前の手順に戻ってください

[%notitle, id=download_ubuntu_new_ubuntu]
=== download_ubuntu_new_install

include::./new_install.adoc[]


endif::[]

ifndef::backend-revealjs[]

.左の画面が表示されている場合はここをクリック
[%collapsible]
====

include::./already_installed.adoc[]

---

include::./next.adoc[]

---

====


.右の画面が表示されている場合はここをクリック
[%collapsible]
====

include::./new_install.adoc[]

---

include::./next.adoc[]

---

====
---

endif::[]





